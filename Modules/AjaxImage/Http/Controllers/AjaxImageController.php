<?php

namespace Modules\AjaxImage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class AjaxImageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('ajaximage::index');
    }

    public function store(Request $request)
    {
        if($request->ajax()) {
            if($request->hasFile('image')) {
                $image = $request->image;
                $filename = time().'.'.$image->getClientOriginalExtension();
                $image->move(public_path('uploads'), $filename);
                return response()->json('/uploads/'.$filename, 200);
            }

            return '';
        }

        throw new MethodNotAllowedHttpException();
    }

}
