<?php

Route::group(['middleware' => 'web', 'prefix' => 'ajaximage', 'namespace' => 'Modules\AjaxImage\Http\Controllers'], function()
{
    Route::get('/', 'AjaxImageController@index');
    Route::post('store','AjaxImageController@store');
});
