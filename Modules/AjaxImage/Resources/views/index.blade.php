@extends('ajaximage::layouts.master')

@section('content')
    <h1>Task: 2</h1>

    <br>

    <form id="imageUploadForm" enctype="multipart/form-data" role="form">
        {!! csrf_field() !!}
        <label for="image">Choose</label>
        <input type="file" name="image" id="image"> <br>
        <button type="button" id="btn_submit">Upload</button>
    </form>
    <br>
    <div id="showImage"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $('#btn_submit').on('click', function() {
            $.ajax({
               url : '/ajaximage/store',
                data: new FormData($('#imageUploadForm')[0]),
                dataType:'json',
                async:false,
                type:'post',
                processData: false,
                contentType: false,
                success: function(response) {

                     $('#showImage').append("<img src='"+response+"' style='width:200px; height:200px;' />");
                    $('#image').val('');
                },
            });
        });
    </script>
@stop
