<!DOCTYPE html>
<html>
<head>
    <style>
        div.gallery {
            border: 1px solid #ccc;
        }

        div.gallery:hover {
            border: 1px solid #777;
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }

        * {
            box-sizing: border-box;
        }

        .responsive {
            padding: 0 6px;
            float: left;
            width: 24.99999%;
        }

        @media only screen and (max-width: 700px){
            .responsive {
                width: 49.99999%;
                margin: 6px 0;
            }
        }

        @media only screen and (max-width: 500px){
            .responsive {
                width: 100%;
            }
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
</head>
<body>

<h2>Search Products  (displays only 15 item)</h2>
<form action="/ebayservice" method="get">
<input type="text" width="500px;" name="title" value="{{ app('request')->get('title') }}"> <br>
<button type="submit">Search</button>
</form>
<br>
<br>

@if(isset($items) &&  $items != null)
    @foreach($items as $item)
        <div class="responsive">
            <div class="gallery">
                <a target="_blank" href="{{ $item->galleryURL }}">
                    <img src="{{ $item->galleryURL }}" alt="Trolltunga Norway" style="width: 200px; height: 200px;" >
                </a>
                <div class="desc"><a href="{{ $item->viewItemURL }}"> {{ $item->title }} </a></div>
            </div>
        </div>
    @endforeach
@endif


<div class="clearfix"></div>


</body>
</html>
