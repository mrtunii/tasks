<?php

Route::group(['middleware' => 'web', 'prefix' => 'ebayservice', 'namespace' => 'Modules\EbayService\Http\Controllers'], function()
{
    Route::get('/', 'EbayServiceController@index');
});
