<?php

namespace Modules\EbayService\Http\Controllers;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Finding\Services;
use \DTS\eBaySDK\Finding\Types;
use \DTS\eBaySDK\Finding\Enums;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class EbayServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $items = null;
        if ($request->input('title')) {

            $service = new Services\FindingService([
                'credentials' => config('ebay.production.credentials'),
                'globalId' => Constants\GlobalIds::US
            ]);
            $Ebayrequest = new Types\FindItemsByKeywordsRequest();

            $Ebayrequest->keywords = $request->input('title');
            $Ebayrequest->paginationInput = new \DTS\eBaySDK\Finding\Types\PaginationInput();
            $Ebayrequest->paginationInput->entriesPerPage = 15;
            $Ebayrequest->paginationInput->pageNumber = 1;
            $response = $service->findItemsByKeywords($Ebayrequest);
            $items = $response->searchResult->item;

        }
        return view('ebayservice::index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('ebayservice::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('ebayservice::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('ebayservice::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
