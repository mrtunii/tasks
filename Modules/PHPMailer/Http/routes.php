<?php

Route::group(['middleware' => 'web', 'prefix' => 'phpmailer', 'namespace' => 'Modules\PHPMailer\Http\Controllers'], function()
{
    Route::get('/', 'PHPMailerController@index');
    Route::post('/sed', 'PHPMailerController@send')->name('mail.send');
});
