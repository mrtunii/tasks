<?php

namespace Modules\PHPMailer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;


class PHPMailerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('phpmailer::index');
    }


    public function send(Request $request)
    {
        $this->validate($request, [
            'to' => 'required|email',
            'body' => 'required|max:200'
        ]);



        $mail = new PHPMailer(true);
        try {

            $mail->isSMTP();
            $mail->Host = env('MAIL_HOST');
            $mail->SMTPAuth = true;
            $mail->Username = env('MAIL_USERNAME');
            $mail->Password = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port = env('MAIL_PORT');

            $mail->setFrom('jesterlin111@gmail.com', 'Task Mailer');
            $mail->addAddress($request->to);


            //Content
            $mail->isHTML();
            $mail->Subject = 'Test Mail';
            $mail->Body    = $request->body;

            $mail->send();
           return 'Sent';
        } catch (Exception $e) {
            dd('Error!', $mail->ErrorInfo);
        }
    }
}
