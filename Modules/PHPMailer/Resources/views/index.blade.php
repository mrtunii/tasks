@extends('phpmailer::layouts.master')

@section('content')
    <h1>ჩაწერეთ და გააგზავნეთ: </h1>
    <br>
    @if(count($errors) > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li style="color:red">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <form action="{{ route('mail.send') }}" method="post">
        {!! csrf_field() !!}

        <input type="text" placeholder="to" name="to"><br>
        <textarea name="body"></textarea>
        <br>
        <input type="submit" value="Send">
    </form>
@stop
