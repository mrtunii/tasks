@extends('facebookuser::layouts.master')

@section('content')
   <h1>User: {{ $user->name }}</h1>
    <ul>
        <li>Name: {{ $user->name }}</li>
        <li>Email: {{ $user->email != null ? $user->email : '' }}</li>
        <li>Avatar: <a href="{{ $user->avatar }}" target="_blank">Click</a> </li>
        <li>ID: {{ $user->id }}</li>
        <li>Profile: <a href="{{ $user->profileUrl }}" target="_blank">Click</a> </li>
    </ul>
@stop
