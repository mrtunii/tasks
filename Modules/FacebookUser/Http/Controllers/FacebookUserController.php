<?php

namespace Modules\FacebookUser\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Socialite\Facades\Socialite;


class FacebookUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('facebookuser::index');
    }



    public function redirect($provider = 'facebook')
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleCallback($provider)
    {
        try{

        $user = Socialite::driver($provider)->user();

        return view('facebookuser::show',compact('user'));

        } catch (\Exception $e) {
            return redirect('facebookuser');
        }
    }
}
