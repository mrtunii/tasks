<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\FacebookUser\Http\Controllers'], function()
{
    Route::get('facebookuser', 'FacebookUserController@index');
    Route::get('auth/{provider}', 'FacebookUserController@redirect');
    Route::get('auth/{provider}/callback', 'FacebookUserController@handleCallback');
});


