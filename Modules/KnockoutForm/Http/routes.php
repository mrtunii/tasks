<?php

Route::group(['middleware' => 'web', 'prefix' => 'knockoutform', 'namespace' => 'Modules\KnockoutForm\Http\Controllers'], function()
{
    Route::get('/', 'KnockoutFormController@index');
});
