@extends('knockoutform::layouts.master')

@section('content')
    <style>
        .validationMessage {
            color:red;
        }
    </style>
    <h1>Task: 7</h1>

    <label for="name">Name</label>
    <input type="text" data-bind="textInput: userName" id="name">
    <br>


    <br>
    <label for="age">Age</label>
    <select id="age" data-bind="options: ages, value: selectedAge,"></select>
    <br>
    <label>Name + Age:</label>
    <span data-bind="text: outputText"></span>

    <br>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout-validation/2.0.3/knockout.validation.min.js"></script>
    <script>
        ko.validation.init({
            registerExtenders: true,
            messagesOnModified: true,
            insertMessages: true,
            parseInputAttributes: true,
            messageTemplate: null
        }, true);
        var viewModel = {
            userName: ko.observable("Oto").extend({ required: true}),
            ages: ko.observableArray(['15', '16', '17']),
            selectedAge: ko.observable(),
        };

        viewModel.outputText = ko.pureComputed(function() {
                return this.userName() + ' is ' + this.selectedAge() + ' years old';
        }, viewModel);

        ko.applyBindings(viewModel);
    </script>
@stop
