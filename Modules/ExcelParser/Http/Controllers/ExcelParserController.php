<?php

namespace Modules\ExcelParser\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Modules\ExcelParser\Entities\ExcelData;

class ExcelParserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = ExcelData::all();
        return view('excelparser::index',compact('data'));
    }

    public function process()
    {
        try{

            ExcelData::truncate();
            Excel::load(public_path('excelFile/data.xlsx'), function($reader) {
               $reader->each(function($row) {
                  $newExcelData = new ExcelData();
                  $newExcelData->name = $row->name;
                  $newExcelData->lastname = $row->lastname;
                  $newExcelData->phone = $row->phone;
                  $newExcelData->save();
               });
            });

            return redirect('/excelparser');
        }catch (\Exception $e) {
            dd($e);
            dd("Can't Process Excel");
        }
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function show()
    {

    }
}
