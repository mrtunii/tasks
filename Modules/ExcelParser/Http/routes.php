<?php

Route::group(['middleware' => 'web', 'prefix' => 'excelparser', 'namespace' => 'Modules\ExcelParser\Http\Controllers'], function()
{
    Route::get('/', 'ExcelParserController@index');
    Route::get('/process','ExcelParserController@process');
});
