<?php

namespace Modules\ExcelParser\Entities;

use Illuminate\Database\Eloquent\Model;

class ExcelData extends Model
{
    protected $table = 'excel_data';
    protected $fillable = [
        'name','lastname','phone'
    ];


}
