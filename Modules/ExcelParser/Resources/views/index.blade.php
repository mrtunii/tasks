<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h1>Task: 4</h1>
<br>
<p><a href="/excelparser/process"> Click here</a> to process Excel File </p>
<br>
<h3>Excel Data:</h3>
<table>
    <tr>
        <th>Name</th>
        <th>Lastname</th>
        <th>Phone</th>
    </tr>
    @foreach($data as $item)
        <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->lastname }}</td>
            <td>{{ $item->phone }}</td>
        </tr>
    @endforeach
</table>

</body>
</html>