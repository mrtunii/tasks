# Tasks

#### 1) ლარაველის დაყენებას და ბაზასთან დაკავშირება:
 შესრულებულია მე-4 დავალებაში

#### 2) jQuery-ით სურათის ატვირთვა:
##### გამოყენება:
- მდებარეობა:
```
   /Modules/AjaxImage
```
- ლინკი: 
```
   http://site.dev/ajaximage
```
#### 3) PHPMailer ბიბლიოთეკით მეილის გაგზავნის მოდული 

##### ინსტალაცია:
- შეცვალეთ Mail პარამეტრები .env ფაილში

##### გამოყენებული ბიბლიოთეკები:
- [PHPMailer](https://github.com/PHPMailer/PHPMailer)

##### გამოყენება:
- მდებარეობა: 
```
/Modules/PHPMailer
```
- ლინკი: 
```
http://site.dev/phpmailer
```

#### 4) სატესტო Excel ფაილის გაპარსვა და შენახვა

##### ინსტალაცია:
- არაფერია დასაყენებელი/შესაცვლელი.
- Excel ფაილი არის /public/excelFile ფოლდერში

##### გამოყენებული ბიბლიოთეკები:
- [Laravel-excel](https://github.com/Maatwebsite/Laravel-Excel)

##### გამოყენება:
- მდებარეობა: 
```
/Modules/ExcelParser
```
- ლინკი: 
```
http://site.dev/excelparser
```

#### 5) Facebook API-ით სატესტო User-ის ინფოს წამოღება

##### ინსტალაცია:
- შეცვალეთ FACEBOOK-ის პარამეტრები .env ფაილში

##### გამოყენებული ბიბლიოთეკები:
- [Socialite](https://github.com/laravel/socialite)

##### გამოყენება:
- მდებარეობა: 
```
/Modules/FacebookUser
```
- ლინკი: 
```
http://site.dev/facebookuser
```

#### 6) Ebay API-ით ნეისმიერი სატესტო რექუესთის გაგზავნა და პასუხის გამოტანა

##### ინსტალაცია:
- შეცვალეთ EBAY-ის პარამეტრები .env ფაილში

##### გამოყენებული ბიბლიოთეკები:
- [Ebay PHP SDK](https://github.com/davidtsadler/ebay-sdk-php)

##### გამოყენება:
- მდებარეობა: 
```
/Modules/EbayService
```
- ლინკი: 
```
http://site.dev/ebayservice
```



#### 7) Knockout.js-ზე მარტივი აპლიკაციის დაწერა

##### გამოყენება:
- მდებარეობა: 
```
/Modules/KnockoutForm
```
- ლინკი: 
```
http://site.dev/knockoutform
```

##### შენიშვნა: ვალიდაცია ხდება მხოლოდ require მეთოდით :) 